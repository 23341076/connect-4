// app\components\connect-4.js

import Component from '@ember/component';

export default Component.extend({
  playing: false,
  winner: undefined,
  draw: false,

  didInsertElement: function() {
    var stage = new createjs.Stage(this.$('#stage')[0]);
    //Draw the game board
    var board = new createjs.Shape();
    var graphics = board.graphics;
    graphics.beginFill('#ffffff');

    //Horizontal lines
    graphics.drawRect(-2, -2, 354, 4);
    graphics.drawRect(-2, 48, 350, 4);
    graphics.drawRect(-2, 98, 350, 4);
    graphics.drawRect(-2, 148, 350, 4);
    graphics.drawRect(-2, 198, 350, 4);
    graphics.drawRect(-2, 248, 350, 4);
    graphics.drawRect(-2, 298, 350, 4);
    graphics.drawRect(-2, 348, 354, 4);

    //Vertical lines
    graphics.drawRect(-2, -2, 4, 304);
    graphics.drawRect(48, -2, 4, 300);
    graphics.drawRect(98, -2, 4, 300);
    graphics.drawRect(148, -2, 4, 300);
    graphics.drawRect(198, -2, 4, 300);
    graphics.drawRect(248, -2, 4, 300);
    graphics.drawRect(298, -2, 4, 300);
    graphics.drawRect(348, -2, 4, 304);

    //Moves the board 10 pixels from the left
    board.x = 2;
    board.y = 2;
    stage.addChild(board);

    //Create the markers
    var markers = {
      'x': [],
      'o': []
    }
    for(var x = 0; x < 21; x++) {
      //Draw the red circle
      var redMarker = new createjs.Shape();
      graphics = redMarker.graphics;
      graphics.beginStroke('#ff0000');
      graphics.beginFill('#ff0000');
      graphics.setStrokeStyle(3);
      graphics.drawCircle(27, 27, 18);
      redMarker.visible = false;
      stage.addChild(redMarker);
      markers.o.push(redMarker);

      //Draw the yellow circleMarker
      var yellowMarker = new createjs.Shape();
      graphics = yellowMarker.graphics;
      graphics.beginStroke('#ffff00');
      graphics.beginFill('#ffff00');
      graphics.setStrokeStyle(3);
      graphics.drawCircle(27, 27, 18);
      yellowMarker.visible = false;
      stage.addChild(yellowMarker);
      markers.x.push(yellowMarker);
    }

    //replace the marker and redraw the stage
    this.set('markers', markers);
    this.set('stage', stage);
  },


    click: function(ev) {
      console.log(ev);
      if(this.get('playing') && !this.get('winner')) {
        if(ev.target.tagName.toLowerCase() == 'canvas' && ev.offsetX >= 10 && ev.offsetY >= 10 && ev.offsetX < 340 && ev.offsetY < 340) {
          var x = Math.floor(ev.offsetX / 50);
          var y = Math.floor(ev.offsetY / 50);
          //console.log(x);
          //console.log(y);
          var state = this.get('state');
          if(!state[x][y]) {
            var player = this.get('player')
            state[x][y] = player;

            var move_count = this.get('moves')[player];
            var marker = this.get('markers')[player][move_count];
            marker.visible = true;
            if (player == 'x') {
              marker.x = x * 50;
              marker.y = y * 50;
            } else {
              marker.x = x * 50;
              marker.y = y * 50;
            }

            this.check_winner();
            this.get('moves')[player] = move_count + 1;
            if (player == 'x') {
              this.set('player', 'o');
            } else {
              this.set('player', 'x');
            }
            this.get('stage').update();
          }

        }
      }
    },

   //checks for winner
   check_winner: function() {
     var patterns = [
         [[0, 0], [0, 1], [0, 2], [0, 3]],
         [[0, 1], [0, 2], [0, 3], [0, 4]],
         [[0, 2], [0, 3], [0, 4], [0, 5]],

         [[1, 0], [1, 1], [1, 2], [1, 3]],
         [[1, 1], [1, 2], [1, 3], [1, 4]],
         [[1, 2], [1, 3], [1, 4], [1, 5]],

         [[2, 0], [2, 1], [2, 2], [2, 3]],
         [[2, 1], [2, 2], [2, 3], [2, 4]],
         [[2, 2], [2, 3], [2, 4], [2, 5]],

         [[3, 0], [3, 1], [3, 2], [3, 3]],
         [[3, 1], [3, 2], [3, 3], [3, 4]],
         [[3, 2], [3, 3], [3, 4], [3, 5]],

         [[4, 0], [4, 1], [4, 2], [4, 3]],
         [[4, 1], [4, 2], [4, 3], [4, 4]],
         [[4, 2], [4, 3], [4, 4], [4, 5]],

         [[5, 0], [5, 1], [5, 2], [5, 3]],
         [[5, 1], [5, 2], [5, 3], [5, 4]],
         [[5, 2], [5, 3], [5, 4], [5, 5]],

         [[6, 0], [6, 1], [6, 2], [6, 3]],
         [[6, 1], [6, 2], [6, 3], [6, 4]],
         [[6, 2], [6, 3], [6, 4], [6, 5]],


         [[0, 0], [1, 0], [2, 0], [3, 0]],
         [[1, 0], [2, 0], [3, 0], [4, 0]],
         [[2, 0], [3, 0], [4, 0], [5, 0]],
         [[3, 0], [4, 0], [5, 0], [6, 0]],

         [[0, 1], [1, 1], [2, 1], [3, 1]],
         [[1, 1], [2, 1], [3, 1], [4, 1]],
         [[2, 1], [3, 1], [4, 1], [5, 1]],
         [[3, 1], [4, 1], [5, 1], [6, 1]],
         [[0, 2], [1, 2], [2, 2], [3, 2]],
         [[1, 2], [2, 2], [3, 2], [4, 2]],
         [[2, 2], [3, 2], [4, 2], [5, 2]],
         [[3, 2], [4, 2], [5, 2], [6, 2]],
         [[0, 3], [1, 3], [2, 3], [3, 3]],
         [[1, 3], [2, 3], [3, 3], [4, 3]],
         [[2, 3], [3, 3], [4, 3], [5, 3]],
         [[3, 3], [4, 3], [5, 3], [6, 3]],
         [[0, 4], [1, 4], [2, 4], [3, 4]],
         [[1, 4], [2, 4], [3, 4], [4, 4]],
         [[2, 4], [3, 4], [4, 4], [5, 4]],
         [[3, 4], [4, 4], [5, 4], [6, 4]],
         [[0, 5], [1, 5], [2, 5], [3, 5]],
         [[1, 5], [2, 5], [3, 5], [4, 5]],
         [[2, 5], [3, 5], [4, 5], [5, 5]],
         [[3, 5], [4, 5], [5, 5], [6, 5]],
         [[0,2], [1,3], [2,4], [3,5]],
         [[0,1], [1,2], [2,3], [3,4]],
         [[1,2], [2,3], [3,4], [4,5]],
         [[0,0], [1,1], [2,2], [3,3]],
         [[1,1], [2,2], [3,3], [4,4]],
         [[2,2], [3,3], [4,4], [5,5]],
         [[1,0], [2,1], [3,2], [4,3]],
         [[2,1], [3,2], [4,3], [5,4]],
         [[3,2], [4,3], [5,4], [6,5]],
         [[2,0], [3,1], [4,2], [5,3]],
         [[3,1], [4,2], [5,3], [6,4]],
         [[3,0], [4,1], [5,2], [6,3]],
         [[0,3], [1,2], [2,1], [3,0]],
         [[0,4], [1,3], [2,2], [3,1]],
         [[1,3], [2,2], [3,1], [4,0]],
         [[0,5], [1,4], [2,3], [3,2]],
         [[1,4], [2,3], [3,2], [4,1]],
         [[2,3], [3,2], [4,1], [5,0]],
         [[1,5], [2,4], [3,3], [4,2]],
         [[2,4], [3,3], [4,2], [5,1]],
         [[3,3], [4,2], [5,1], [6,0]],
         [[2,5], [3,4], [4,3], [5,2]],
         [[3,4], [4,3], [5,2], [6,1]],
         [[3,5], [4,4], [5,3], [6,2]],
     ];

     var state = this.get('state');
     var markers = this.get('markers');
     for(var pidx = 0; pidx < patterns.length; pidx++) {
       var pattern = patterns[pidx];
       var winner = state[pattern[0][0]];
       if(winner) {
         for(var idx = 1; idx < pattern.length; idx++) {
           if(winner != state[pattern[idx][0]][pattern[idx][1]]) {
             winner = undefined;
             break;
           }
         }
       }
       if(!this.get('winner')) {
         var draw = true;
         for(var x = 0; x <= 2; x++) {
           for(var y = 0; y <= 2; y++) {
             if(!state[x][y]) {
               draw = false
               break;
             }
           }
         }
         this.set('draw', draw);
       }
     }
   },

    actions: {
      start: function() {
        //alert("test")
        this.set('playing', true);
        this.set('winner', undefined);
        this.set('draw', false);
        this.set('state', [
          [undefined, undefined, undefined, undefined, undefined, undefined],
          [undefined, undefined, undefined, undefined, undefined, undefined],
          [undefined, undefined, undefined, undefined, undefined, undefined],
          [undefined, undefined, undefined, undefined, undefined, undefined],
          [undefined, undefined, undefined, undefined, undefined, undefined],
          [undefined, undefined, undefined, undefined, undefined, undefined],
          [undefined, undefined, undefined, undefined, undefined, undefined]
        ]);
        this.set('moves', {'x': 0, 'o': 0});
        this.set('player', 'x');
        var markers = this.get('markers');
        for(var idx = 0; idx < 21; idx++) {
          markers.x[idx].visible = false;
          markers.o[idx].visible = false;
        }
        //update the drawing
        this.get('stage').update();
      }
    }

    });
